package br.com.fiap.solution.teste.crud;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.CursoDAO;
import br.com.fiap.solution.dao.impl.ProfessorDAO;
import br.com.fiap.solution.dao.impl.TurmaDAO;
import br.com.fiap.solution.entity.Curso;
import br.com.fiap.solution.entity.Professor;
import br.com.fiap.solution.entity.Turma;
import br.com.fiap.solution.entity.enums.EModalidade;
import br.com.fiap.solution.entity.enums.ETipoCurso;
import br.com.fiap.solution.util.DadosTeste;

public class TurmaTeste {

	private TurmaDAO turmaDao;
	private CursoDAO cursoDAO;
	private ProfessorDAO professorDao;
	
	public TurmaTeste(EntityManager em) {
		this.turmaDao = new TurmaDAO(em);
		this.cursoDAO = new CursoDAO(em);
		this.professorDao = new ProfessorDAO(em);
	}
	
	public void cadastrarListaTurma() throws Exception {

		DadosTeste.logarTitulo("CADASTRO TURMA");

		Curso cursoOnline = cursoDAO.buscarPorNomeTipoEModalidade("FULL STACK DEVELOPMENT", 
				ETipoCurso.MBA, EModalidade.ONLINE).get(0);
		Curso cursoPresencial = cursoDAO.buscarPorNomeTipoEModalidade("ARQUITETURA DE SOLUÇÕES", 
				ETipoCurso.MBA, EModalidade.PRESENCIAL).get(0);
		
		Calendar dtInicioOnline = new GregorianCalendar(2021, 03, 26);
		Calendar dtInicioPresencial = new GregorianCalendar(2021, 03, 30);
		List<Professor> lstProfessor = professorDao.listar();
		
		Turma turmaOnline = new Turma("2CJO", dtInicioOnline, cursoOnline.getDuracaoMeses(), cursoOnline, lstProfessor);
		Turma turmaOnline2 = new Turma("2DJO", dtInicioOnline, cursoOnline.getDuracaoMeses(), cursoOnline, lstProfessor);
		Turma turmaPresencial = new Turma("2AJO", dtInicioPresencial, cursoPresencial.getDuracaoMeses(), cursoPresencial, lstProfessor);
		Turma turmaPresencial2 = new Turma("2BJO", dtInicioPresencial, cursoPresencial.getDuracaoMeses(), cursoPresencial, lstProfessor);
		
		turmaDao.cadastrar(turmaOnline);
		turmaDao.cadastrar(turmaOnline2);
		turmaDao.cadastrar(turmaPresencial);
		turmaDao.cadastrar(turmaPresencial2);
		
		DadosTeste.logarLinha();
	}

	public void listarTurmas() {
		DadosTeste.logarTitulo("CONSULTA DAS TURMAS CADASTRADAS");
		
		List<Turma> lstTurma = turmaDao.listar();
		for (Turma turma : lstTurma) {
			System.out.println(turma.toString());
		}
		
		DadosTeste.logarLinha();
	}

	public void excluirTurma() throws Exception {
		
		Turma turma = turmaDao.buscarUltimoRegistro();
		DadosTeste.logarTitulo("EXCLUIR TURMA COM ID " + turma.getId());
		
		turmaDao.excluir(turma.getId());
		
		DadosTeste.logarLinha();
	}


	public void atualizarNomeTurma() throws Exception {
		Turma turma = turmaDao.buscarUltimoRegistro();
		DadosTeste.logarTitulo("ATUALIZAR NOME DA TURMA COM ID " + turma.getId());
		
		turma.setNomeTurma("3DCO");
		turmaDao.atualizar(turma);
		
		DadosTeste.logarLinha();
	}

}
