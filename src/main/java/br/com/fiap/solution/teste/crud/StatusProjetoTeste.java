package br.com.fiap.solution.teste.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.StatusProjetoDAO;
import br.com.fiap.solution.entity.StatusProjeto;
import br.com.fiap.solution.util.DadosTeste;

public class StatusProjetoTeste {

	private StatusProjetoDAO statusProjetoDao;
	
	public StatusProjetoTeste(EntityManager em) {
		this.statusProjetoDao = new StatusProjetoDAO(em);
	}
	
	public void cadastrarListaStatusProjeto() throws Exception {

		DadosTeste.logarTitulo("CADASTRO DE STATUS DE PROJETO");
		List<StatusProjeto> lstStatusProjeto = new ArrayList<StatusProjeto>();
		lstStatusProjeto.add(new StatusProjeto("EM ANÁLISE"));
		lstStatusProjeto.add(new StatusProjeto("NOVO"));
		lstStatusProjeto.add(new StatusProjeto("LIBERAR PARA AVALIAÇÃO"));
		lstStatusProjeto.add(new StatusProjeto("PENDENTE CORREÇÃO"));
		lstStatusProjeto.add(new StatusProjeto("PUBLICADO"));
		lstStatusProjeto.add(new StatusProjeto("REVISADO"));
		lstStatusProjeto.add(new StatusProjeto("CADASTRO INCORRETO PARA DELETE"));
		lstStatusProjeto.add(new StatusProjeto("CADASTRO INCORRETO PARA ATUALIZACAO"));

		for (StatusProjeto status : lstStatusProjeto) {
			statusProjetoDao.cadastrar(status);
		}
		DadosTeste.logarLinha();
	}

	public void listarStatusProjeto() {
		DadosTeste.logarTitulo("CONSULTA DOS STATUS DE PROJETO CADASTRADOS NA BASE DE DADOS");
		List<StatusProjeto> lstStatusProjeto = statusProjetoDao.listar();
		for (StatusProjeto status : lstStatusProjeto) {
			System.out.println(status.toString());
		}
		DadosTeste.logarLinha();
	}

	public void excluirStatusComId(int id) throws Exception {
		DadosTeste.logarTitulo("EXCLUIR STATUS DE PROJETO COM ID");
		statusProjetoDao.excluir(id);
		DadosTeste.logarLinha();
	}

	public void atualizarStatusComId(int id, String descNovoStatus) throws Exception {
		DadosTeste.logarTitulo("ATUALIZAR STATUS DE PROJETO COM ID");
		StatusProjeto status = statusProjetoDao.buscarPorChave(id);
		status.setDescricao(descNovoStatus);
		statusProjetoDao.atualizar(status);
		DadosTeste.logarLinha();
	}
}
