package br.com.fiap.solution.teste.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.AlunoDAO;
import br.com.fiap.solution.dao.impl.GrupoDAO;
import br.com.fiap.solution.entity.Aluno;
import br.com.fiap.solution.entity.Grupo;
import br.com.fiap.solution.util.DadosTeste;

public class GrupoTeste {

	private GrupoDAO grupoDao;
	private AlunoDAO alunoDao;
	
	public GrupoTeste(EntityManager em) {
		this.grupoDao = new GrupoDAO(em);
		this.alunoDao = new AlunoDAO(em);
	}
	
	public void cadastrarListaGrupo() throws Exception {

		DadosTeste.logarTitulo("CADASTRO GRUPOS");

		List<Aluno> lstAluno = alunoDao.listar();
		List<Aluno> lstAluno1 = new ArrayList<Aluno>();
		List<Aluno> lstAluno2 = new ArrayList<Aluno>();
		List<Aluno> lstAluno3 = new ArrayList<Aluno>();
		
		lstAluno1.add(lstAluno.get(0));
		lstAluno1.add(lstAluno.get(1));
		
		lstAluno2.add(lstAluno.get(2));
		lstAluno2.add(lstAluno.get(3));
		
		lstAluno3.add(lstAluno.get(4));
		lstAluno3.add(lstAluno.get(5));
		
		lstAluno3.add(lstAluno.get(6));
		
		Grupo grupo1 = new Grupo("Grupo1", lstAluno1);
		Grupo grupo2 = new Grupo("Grupo2", lstAluno2);
		Grupo grupo3 = new Grupo("Grupo3", lstAluno3);
		Grupo grupo4 = new Grupo("Grupo4", lstAluno3);
		Grupo grupo5 = new Grupo("GrupoParaExcluir", new ArrayList<Aluno>());
		Grupo grupo6 = new Grupo("GrupoParaAtualizar", new ArrayList<Aluno>());
		
		grupoDao.cadastrar(grupo1);
		grupoDao.cadastrar(grupo2);
		grupoDao.cadastrar(grupo3);
		grupoDao.cadastrar(grupo4);
		grupoDao.cadastrar(grupo5);
		grupoDao.cadastrar(grupo6);

		DadosTeste.logarLinha();
	}

	public void listarAlunosGrupoPorId(int id) {
		
		DadosTeste.logarTitulo("CONSULTA ALUNOS DO GRUPO COM ID " + id);

		Grupo grupo = grupoDao.buscarPorChave(id);
		
		for (Aluno aluno : grupo.getLstAluno()) {
			System.out.println(aluno.toString());
		}
		
		DadosTeste.logarLinha();
	}

	public void excluir() throws Exception {
		int id = grupoDao.buscarUltimoRegistro().getId();
		
		DadosTeste.logarTitulo("EXCLUIR GRUPO COM ID" + id);
		grupoDao.excluir(id);
		
		DadosTeste.logarLinha();
	}

	public void atualizar() throws Exception {
		Grupo grupo = grupoDao.buscarUltimoRegistro();
		
		DadosTeste.logarTitulo("ATUALIZAR NOME DO GRUPO COM ID" + grupo.getId());
		grupo.setNome("GrupoUp2Date");
		grupoDao.atualizar(grupo);
		
		DadosTeste.logarLinha();
	}
}
