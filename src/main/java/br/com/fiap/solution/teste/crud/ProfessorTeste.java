package br.com.fiap.solution.teste.crud;

import java.util.GregorianCalendar;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.ProfessorDAO;
import br.com.fiap.solution.entity.Professor;
import br.com.fiap.solution.entity.Usuario;
import br.com.fiap.solution.entity.enums.EGenero;
import br.com.fiap.solution.exception.DBCommitException;
import br.com.fiap.solution.util.DadosTeste;

public class ProfessorTeste {

	private ProfessorDAO professorDao;

	public ProfessorTeste(EntityManager em) {
		this.professorDao = new ProfessorDAO(em);
	}
	
	public void cadastrarProfessores() throws DBCommitException {
		DadosTeste.logarTitulo("CADASTRO DE PROFESSORES");
		
		Usuario usuario1 = new Usuario("quinca", "senha");
		Usuario usuario2 = new Usuario("teka", "sen@10");
		
		Professor professor1 = new Professor("Joaquim Rodrigues", new GregorianCalendar(1957, 12, 22),
				"joaquim.rodrigues@gmail.com", "https://www.linkedin.com/in/joaquimrodrigues/", EGenero.MASCULINO, null,
				new GregorianCalendar(2015, 01, 20), usuario1);
		Professor professor2 = new Professor("Terezinha Alexandre", new GregorianCalendar(1985, 07, 06),
				"terezinha.ale@gmail.com", "https://www.linkedin.com/in/terezinhaalexandre/", EGenero.FEMININO, null,
				new GregorianCalendar(2018, 03, 06), usuario2);
		
		professorDao.cadastrar(professor1);
		professorDao.cadastrar(professor2);

		DadosTeste.logarLinha();
	}

	public void consultarProfessorPorId(int id) throws Exception {
		DadosTeste.logarTitulo("CONSULTAR PROFESSOR POR ID");
		Professor prof = professorDao.buscarPorChave(id);
		System.out.println(prof.toString());

	}

	public void atualizarProfessor() throws Exception {
		DadosTeste.logarTitulo("ATUALIZAR PROFESSOR");
		Professor prof = professorDao.buscarPorChave(2);
		prof.setNome("Terezinha Alexandre Rodrigues");
		professorDao.atualizar(prof);
		DadosTeste.logarLinha();
	}

	public void excluirProfessor() throws Exception {
		DadosTeste.logarTitulo("EXCLUIR PROFESSOR");
		/** O professor não será removido da base, apenas adicionado a data de
		 desligamento, o que representa que ele não está mais disponível **/
		Professor prof = professorDao.buscarPorChave(2);
		prof.setDtDesligamento(new GregorianCalendar());
		professorDao.atualizar(prof);
		DadosTeste.logarLinha();
	}

	public void listarProfessor() {
		DadosTeste.logarTitulo("LISTAR TODOS PROFESSORES DISPONÍVEIS");
//		Mostra apenas os professores que tem a data de desligamento maior que a data atual ou que está nulo
		 for (Professor prof : professorDao.listar()) {
			 System.out.println(prof.toString());
		 }
		DadosTeste.logarLinha();
	}

}
