package br.com.fiap.solution.teste.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.CursoDAO;
import br.com.fiap.solution.entity.Curso;
import br.com.fiap.solution.entity.enums.EModalidade;
import br.com.fiap.solution.entity.enums.ETipoCurso;
import br.com.fiap.solution.util.DadosTeste;

public class CursoTeste {

	public CursoDAO cursoDao;
	
	public CursoTeste(EntityManager em) {
		this.cursoDao = new CursoDAO(em);
	}
	
	public void cadastrarListaCurso() throws Exception {

		DadosTeste.logarTitulo("CADASTRO CURSOS DISPONÍVEIS");

		List<Curso> lstCurso = new ArrayList<Curso>();
		lstCurso.add(new Curso("ARQUITETURA DE SOLUÇÕES", "Arquitetar soluções de aplicações", ETipoCurso.MBA,EModalidade.ONLINE, 12));
		lstCurso.add(new Curso("ARQUITETURA DE SOLUÇÕES", "Arquitetar soluções de aplicações", ETipoCurso.MBA,EModalidade.PRESENCIAL, 15));
		lstCurso.add(new Curso("BIG DATA - DATA SCIENCE", "Curso de tecnologia", ETipoCurso.MBA, EModalidade.ONLINE, 12));
		lstCurso.add(new Curso("DIGITAL DATA MARKETING", "Negócio", ETipoCurso.MBA, EModalidade.PRESENCIAL, 15));
		lstCurso.add(new Curso("FULL STACK DEVELOPMENT", "Curso de tecnologia", ETipoCurso.MBA, EModalidade.ONLINE, 12));
		lstCurso.add(new Curso("FULL STACK DEVELOPMENT", "Curso de tecnologia", ETipoCurso.MBA, EModalidade.PRESENCIAL, 15));
		lstCurso.add(new Curso("CADASTRO INCORRETO PARA DELETE", "Curso para excluir", ETipoCurso.MBA, EModalidade.ONLINE, 12));
		lstCurso.add(new Curso("CADASTRO INCORRETO PARA ATUALIZACAO", "Curso para atualizar", ETipoCurso.MBA, EModalidade.PRESENCIAL, 15));

		for (Curso curso : lstCurso) {
			cursoDao.cadastrar(curso);
		}

		DadosTeste.logarLinha();
	}

	public void listarCursos() {
		DadosTeste.logarTitulo("CONSULTA CURSOS CADASTRADOS NA BASE DE DADOS");
		List<Curso> lstCurso = cursoDao.listar();
		for (Curso curso : lstCurso) {
			System.out.println(curso.toString());
		}
		DadosTeste.logarLinha();
	}

	public void excluirComId(int id) throws Exception {
		DadosTeste.logarTitulo("EXCLUIR CURSO COM ID" + id);
		cursoDao.excluir(id);
		DadosTeste.logarLinha();
	}

	public void atualizarNomeCursoComId(int id, String novoNomeCurso) throws Exception {
		DadosTeste.logarTitulo("ATUALIZAR NOME DO CURSO COM ID" + id);
		Curso curso = cursoDao.buscarPorChave(id);
		curso.setNome(novoNomeCurso);
		cursoDao.atualizar(curso);
		DadosTeste.logarLinha();
	}
}
