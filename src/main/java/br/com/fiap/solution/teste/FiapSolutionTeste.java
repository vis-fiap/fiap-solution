package br.com.fiap.solution.teste;

import javax.persistence.EntityManager;

import br.com.fiap.solution.singleton.EMSingleton;
import br.com.fiap.solution.teste.crud.AlunoTeste;
import br.com.fiap.solution.teste.crud.CursoTeste;
import br.com.fiap.solution.teste.crud.FeedbackTeste;
import br.com.fiap.solution.teste.crud.GrupoTeste;
import br.com.fiap.solution.teste.crud.ProfessorTeste;
import br.com.fiap.solution.teste.crud.ProjetoTeste;
import br.com.fiap.solution.teste.crud.StatusProjetoTeste;
import br.com.fiap.solution.teste.crud.TemaTeste;
import br.com.fiap.solution.teste.crud.TurmaTeste;

public class FiapSolutionTeste {

	public static void main(String[] args) throws Exception {
		
		EntityManager em = EMSingleton.getInstance().createEntityManager();
		
		// STATUS PROJETO
		StatusProjetoTeste statusProjeto = new StatusProjetoTeste(em);
		
		try {
			statusProjeto.cadastrarListaStatusProjeto();
			statusProjeto.excluirStatusComId(6);
			statusProjeto.atualizarStatusComId(7, "CANCELADO");
			statusProjeto.listarStatusProjeto();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// TEMA
		TemaTeste tema = new TemaTeste(em);
		
		try {
			tema.cadastrarTemas();
			tema.atualizarTema();
			tema.excluirTema();
			tema.listarTemas();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// CURSO
		CursoTeste curso = new CursoTeste(em);
		try {
			curso.cadastrarListaCurso();
			curso.excluirComId(7);
			curso.atualizarNomeCursoComId(8, "DIGITAL BUSINESS");
			curso.listarCursos();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// PROFESSOR
		ProfessorTeste professor = new ProfessorTeste(em);
		try {
			professor.cadastrarProfessores();
			professor.consultarProfessorPorId(1);
			professor.atualizarProfessor();
			professor.excluirProfessor();
			professor.listarProfessor();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// TURMA
		TurmaTeste turma = new TurmaTeste(em);
		try {
			// CRUD de turma
			turma.cadastrarListaTurma();
			turma.atualizarNomeTurma();
			turma.excluirTurma();
			turma.listarTurmas();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// ALUNO
		AlunoTeste aluno = new AlunoTeste(em);
		try {
			aluno.cadastrarAlunos();
			aluno.consultarAlunoPorRM();
			aluno.listarAlunosPorTurma();
			aluno.atualizarAluno();
			aluno.excluirAluno();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// GRUPO
		GrupoTeste grupo = new GrupoTeste(em);
		try {
			grupo.cadastrarListaGrupo();
			grupo.excluir();
			grupo.atualizar();
			grupo.listarAlunosGrupoPorId(1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// PROJETO
		ProjetoTeste projeto = new ProjetoTeste(em);
		try {
			projeto.cadastrarListaProjeto();
			projeto.excluir();
			projeto.listar();
			projeto.atualizarStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//FEEDBACK
		FeedbackTeste feedback = new FeedbackTeste(em);
		
		try {
			
			feedback.cadastrarListaFeedback();
			feedback.excluir();
			feedback.atualizar();
			feedback.listarFeedbackPorProjeto();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
