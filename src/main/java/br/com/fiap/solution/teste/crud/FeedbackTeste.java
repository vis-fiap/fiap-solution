package br.com.fiap.solution.teste.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.FeedbackDAO;
import br.com.fiap.solution.dao.impl.ProjetoDAO;
import br.com.fiap.solution.dao.impl.UsuarioDAO;
import br.com.fiap.solution.entity.Feedback;
import br.com.fiap.solution.entity.Projeto;
import br.com.fiap.solution.entity.Usuario;
import br.com.fiap.solution.util.DadosTeste;

public class FeedbackTeste {
	
	private FeedbackDAO feedbackDao;
	private ProjetoDAO projetoDao;
	private UsuarioDAO usuarioDao;
	
	public FeedbackTeste(EntityManager em) {
		feedbackDao = new FeedbackDAO(em);
		projetoDao = new ProjetoDAO(em);
		usuarioDao = new UsuarioDAO(em);
	}

	public void cadastrarListaFeedback() throws Exception {

		DadosTeste.logarTitulo("CADASTRO FEEDBACKS");

		Projeto projeto = projetoDao.buscarPorChave(1);
		Projeto projeto2 = projetoDao.buscarPorChave(2);
		
		List<Usuario> lstUsuario = usuarioDao.listar();
		
		List<Feedback> lstFeedback = new ArrayList<Feedback>();
		lstFeedback.add(new Feedback(5,"Projeto com apresentação clara", lstUsuario.get(0), projeto));
		lstFeedback.add(new Feedback(2,"A ideia precisa ser validada com os possíveis usuários.", lstUsuario.get(0), projeto));
		lstFeedback.add(new Feedback(3,"Projeto com avaliação média", lstUsuario.get(0), projeto));
		lstFeedback.add(new Feedback(5,"Projeto sensacional!", lstUsuario.get(1), projeto2));
		lstFeedback.add(new Feedback(4,"Necessário explicar o retorno monetário da solução",lstUsuario.get(1), projeto2));
		lstFeedback.add(new Feedback(5, lstUsuario.get(2), projeto2));

		for (Feedback feedback : lstFeedback) {
			feedbackDao.cadastrar(feedback);
		}

		DadosTeste.logarLinha();
	}

	public void listarFeedbackPorProjeto() {
		
		Projeto projeto = projetoDao.buscarPorChave(1);
		DadosTeste.logarTitulo("CONSULTA FEEDBACK DO PROJETO COM ID " + projeto.getId());

		List<Feedback> lstFeedback = feedbackDao.buscarPorProjeto(projeto);
		for (Feedback feedback : lstFeedback) {
			System.out.println(feedback.toString());
		}
		
		DadosTeste.logarLinha();
	}

	public void excluir() throws Exception {
		int id = feedbackDao.buscarUltimoRegistro().getId();
		
		DadosTeste.logarTitulo("EXCLUIR FEEDBACK COM ID" + id);
		feedbackDao.excluir(id);
		
		DadosTeste.logarLinha();
	}

	public void atualizar() throws Exception {
		Feedback feedback = feedbackDao.buscarUltimoRegistro();
		
		DadosTeste.logarTitulo("ATUALIZAR NOTA DO FEEDBACK COM ID" + feedback.getId());
		feedback.setNota(feedback.getNota() > 1?1:5);
		feedbackDao.atualizar(feedback);
		
		DadosTeste.logarLinha();
	}
}
