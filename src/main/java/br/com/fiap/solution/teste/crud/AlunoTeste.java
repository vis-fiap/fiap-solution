package br.com.fiap.solution.teste.crud;

import java.util.GregorianCalendar;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.AlunoDAO;
import br.com.fiap.solution.dao.impl.TurmaDAO;
import br.com.fiap.solution.entity.Aluno;
import br.com.fiap.solution.entity.Turma;
import br.com.fiap.solution.entity.Usuario;
import br.com.fiap.solution.entity.enums.EGenero;
import br.com.fiap.solution.exception.DBCommitException;
import br.com.fiap.solution.util.DadosTeste;

public class AlunoTeste {

	private AlunoDAO alunoDao;
	private TurmaDAO turmaDao;

	public AlunoTeste(EntityManager em) {
		alunoDao = new AlunoDAO(em);
		turmaDao = new TurmaDAO(em);
	}
	
	public Turma consultarTurma(String turma) {
		return turmaDao.buscarPorNome(turma).get(0);
	}

	public void cadastrarAlunos() throws DBCommitException {
		DadosTeste.logarTitulo("CADASTRO DE ALUNOS");

		Turma turma = consultarTurma("2CJO");
		
		Usuario usuario1 = new Usuario("LucaSouza", "senha");
		Usuario usuario2 = new Usuario("Thomas", "senha@123");
		Usuario usuario3 = new Usuario("Vih", "senha@123");
		Usuario usuario4 = new Usuario("Vivi", "senha@123");
		Usuario usuario5 = new Usuario("Biel", "senha@123");
		Usuario usuario6 = new Usuario("Jess", "senha@123");
		Usuario usuario7 = new Usuario("Marlllon", "senha@123");
		Usuario usuario8 = new Usuario("Mikke", "senha@123");

		alunoDao.cadastrar(new Aluno("Lucas de Souza da Silva", new GregorianCalendar(1992, 8, 22),
				"lucas_0111@hotmail.com", "", EGenero.MASCULINO, 348547, turma, usuario1));

		alunoDao.cadastrar(new Aluno("Thomas Corres Melachos", new GregorianCalendar(1992, 8, 22),
				"tmelachos@gmail.com", "", EGenero.MASCULINO, 348549, turma, usuario2));

		alunoDao.cadastrar(
				new Aluno("Vitória Alexandre Rodrigues", new GregorianCalendar(1996, 3, 6), "vi_ale10@hotmail.com",
						"www.linkedin.com/in/vitoriaalexandrerodrigues", EGenero.FEMININO, 341349, turma, usuario3));

		alunoDao.cadastrar(
				new Aluno("Vivian Alexandre Rodrigues", new GregorianCalendar(1996, 3, 6), "vis.ale10@gmail.com",
						"www.linkedin.com/in/vivianalexandrerodrigues/", EGenero.FEMININO, 341350, turma, usuario4));

		alunoDao.cadastrar(new Aluno("Gabriel Fernandes", new GregorianCalendar(1992, 8, 22), "biel.fer@gmail.com",
				"www.linkedin.com/in/bielfernandes/", EGenero.MASCULINO, 354124, turma, usuario5));

		alunoDao.cadastrar(new Aluno("Jessica Ferreira", new GregorianCalendar(1992, 8, 22), "jeh.fer@gmail.com",
				"www.linkedin.com/in/jehferreira/", EGenero.FEMININO, 347895, turma, usuario6));
		
		alunoDao.cadastrar(new Aluno("Marlon Montriz", new GregorianCalendar(1998, 1, 07), "marlon.montriz@gmail.com",
				"www.linkedin.com/in/marlonmontriz/", EGenero.MASCULINO, 656454, turma, usuario7));
		
		alunoDao.cadastrar(new Aluno("Mike Tyson Schwarzenegger Pradella", new GregorianCalendar(1986, 5, 28), "pradella.mike@gmail.com",
				"www.linkedin.com/in/miketyson/", EGenero.MASCULINO, 213243, turma, usuario8));

		DadosTeste.logarLinha();
	}

	public void consultarAlunoPorRM() throws Exception {
		DadosTeste.logarTitulo("CONSULTAR ALUNO POR RM");
		Aluno aluno = alunoDao.consultarPorRM(341349).get(0);
		System.out.println(aluno.toString());
		DadosTeste.logarLinha();

	}

	public void atualizarAluno() throws Exception {
		DadosTeste.logarTitulo("ATUALIZAR ALUNO");
		Aluno aluno = alunoDao.buscarUltimoRegistro();
		aluno.setDtNascimento(new GregorianCalendar(1996, 06, 07));
		alunoDao.atualizar(aluno);
		DadosTeste.logarLinha();
	}

	public void excluirAluno() throws Exception {
		DadosTeste.logarTitulo("EXCLUIR ALUNO");
		Aluno aluno = alunoDao.buscarUltimoRegistro();
		alunoDao.excluir(aluno.getId());
		DadosTeste.logarLinha();
	}

	public void listarAlunosPorTurma() {
		DadosTeste.logarTitulo("LISTAR TODOS ALUNOS DA TURMA 2CJO");
		Turma turma = consultarTurma("2CJO");
		for (Aluno aluno : alunoDao.listarAlunosPorTurma(turma)) {
			System.out.println(aluno.getNome());
		}
		DadosTeste.logarLinha();
	}

}
