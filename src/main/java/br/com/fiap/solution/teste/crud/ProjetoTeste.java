package br.com.fiap.solution.teste.crud;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.GrupoDAO;
import br.com.fiap.solution.dao.impl.ProjetoDAO;
import br.com.fiap.solution.dao.impl.StatusProjetoDAO;
import br.com.fiap.solution.dao.impl.TemaDAO;
import br.com.fiap.solution.entity.Grupo;
import br.com.fiap.solution.entity.Projeto;
import br.com.fiap.solution.entity.StatusProjeto;
import br.com.fiap.solution.entity.Tema;
import br.com.fiap.solution.util.DadosTeste;

public class ProjetoTeste {

	private ProjetoDAO projetoDao;
	private StatusProjetoDAO statusProjetoDao;
	private GrupoDAO grupoDao;
	private TemaDAO temaDao;
	
	public ProjetoTeste(EntityManager em) {
		this.projetoDao = new ProjetoDAO(em);
		this.statusProjetoDao = new StatusProjetoDAO(em);
		this.grupoDao = new GrupoDAO(em);
		this.temaDao = new TemaDAO(em);
	}

	public void cadastrarListaProjeto() throws Exception {

		List<Projeto> lstProjeto = new ArrayList<Projeto>();
		
		List<Tema> lstTema = temaDao.listar();
		List<Grupo> lstGrupo = grupoDao.listar();
		
		List<StatusProjeto> lstStatusProjeto = statusProjetoDao.buscarPorDescricao("NOVO");
		StatusProjeto status = lstStatusProjeto.get(0);
		
		DadosTeste.logarTitulo("CADASTRO DE PROJETOS");
		
		lstProjeto.add(new Projeto("TodosPodem", "Projeto de inclusão.", 
				lstTema.get(0), true, "https://github.com/fiap/todos-podem", "https://1drv.ms/w/s!AipIgf2faZcK3zyRDEmKknuaXTQb?e=LyiY7H", status, lstGrupo.get(0)));
		
		lstProjeto.add(new Projeto("AcessoAberto", "Todos pelo acesso igualitário.",
				lstTema.get(0), true, "https://1drv.ms/w/s!t4erterycK3zyRDEmKknuaXTQb?e=LyiY7H", status, lstGrupo.get(1)));
		
		lstProjeto.add(new Projeto("AgroGric", "Legumes mais saudaveis", 
				lstTema.get(4), false, "https://github.com/fiap/agro-gric", "https://1drv.ms/w/s!sdfdssdK3zyRDEmKknuaXTQb?e=LyiY7H", status, lstGrupo.get(2)));
		
		lstProjeto.add(new Projeto("Move Move", "Se movimente no espaço aéreo.", 
				lstTema.get(1), false, "https://1drv.ms/w/s!AipIgf2faZtuhvbnvcbvEmKknuaXTQb?e=Ly222", status, lstGrupo.get(3)));
		
		lstProjeto.add(new Projeto("O amanhã hoje", "Porque deixar para amanhã o que pode ter hoje.", 
				lstTema.get(5), true, "https://github.com/fiap/o-amanha-hoje", "https://1drv.ms/w/syuydsdff2faZcK3zyRDEmKknuaXTQb?e=LyiY7H", status, lstGrupo.get(4)));
		
		for (Projeto projeto : lstProjeto) {
			projetoDao.cadastrar(projeto);
		}

		DadosTeste.logarLinha();
	}

	public void listar() {
		DadosTeste.logarTitulo("CONSULTA PROJETOS CADASTRADOS NA BASE DE DADOS");
		List<Projeto> lstProjeto = projetoDao.listar();
		for (Projeto projeto : lstProjeto) {
			System.out.println(projeto.toString());
		}
		DadosTeste.logarLinha();
	}

	public void excluir() throws Exception {
		int id = projetoDao.buscarUltimoRegistro().getId();
		
		DadosTeste.logarTitulo("EXCLUIR PROJETO COM ID" + id);
		projetoDao.excluir(id);
		
		DadosTeste.logarLinha();
	}

	public void atualizarStatus() throws Exception {
		
		Projeto projeto = projetoDao.buscarUltimoRegistro();
		DadosTeste.logarTitulo("ATUALIZAR DESCRICAO DO PROJETO COM ID" + projeto.getId());
		
		projeto.setStatus(statusProjetoDao.buscarPorChave(2));
		projetoDao.atualizar(projeto);
		
		DadosTeste.logarLinha();
	}
}
