package br.com.fiap.solution.teste.crud;

import javax.persistence.EntityManager;

import br.com.fiap.solution.dao.impl.TemaDAO;
import br.com.fiap.solution.entity.Tema;
import br.com.fiap.solution.util.DadosTeste;

public class TemaTeste {

	public TemaDAO temaDao;

	public TemaTeste(EntityManager em) {
		temaDao = new TemaDAO(em);
	}
	
	public void cadastrarTemas() throws Exception {
		DadosTeste.logarTitulo("CADASTRO DE TEMA DE PROJETO");

		temaDao.cadastrar(new Tema("Acessibilidade"));
		temaDao.cadastrar(new Tema("Mobilidade"));
		temaDao.cadastrar(new Tema("Sustentabilidade"));
		temaDao.cadastrar(new Tema("Saúde"));
		temaDao.cadastrar(new Tema("Agricultura"));
		temaDao.cadastrar(new Tema("Internet das Coisas (IT)"));
		temaDao.cadastrar(new Tema("Inteligência Artificial (IA)"));
		temaDao.cadastrar(new Tema("Tema para atualizar"));
		temaDao.cadastrar(new Tema("Tema para excluir"));

		DadosTeste.logarLinha();
	}

	public void atualizarTema() throws Exception {
		DadosTeste.logarTitulo("ATUALIZAR TEMA DE PROJETO 8");
		temaDao.atualizar(new Tema(8, "Educação"));
		DadosTeste.logarLinha();
	}

	public void excluirTema() throws Exception {
		DadosTeste.logarTitulo("EXCLUIR TEMA DE PROJETO 9");
		temaDao.excluir(9);
		DadosTeste.logarLinha();
	}

	public void listarTemas() {
		DadosTeste.logarTitulo("LISTAR TODOS OS TEMAS DISPONÍVEIS");
		for (Tema tema : temaDao.listar()) {
			System.out.println(tema.toString());
		}
		DadosTeste.logarLinha();
	}

}
