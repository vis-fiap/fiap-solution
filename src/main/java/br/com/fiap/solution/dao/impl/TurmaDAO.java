package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Turma;

public class TurmaDAO extends GenericDAO<Turma, Integer> {

	public TurmaDAO(EntityManager em) {
		super(em);
	}
	
	public List<Turma> buscarPorNome(String nome) {
		
		String consultaSql = "select t from Turma t "
				+"where t.nomeTurma = :nomeTurma";
		
		return this.em.createQuery(consultaSql, Turma.class)
				.setParameter("nomeTurma", nome)
				.setMaxResults(1).getResultList();
		
	}

}
