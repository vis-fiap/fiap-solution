package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Curso;
import br.com.fiap.solution.entity.enums.EModalidade;
import br.com.fiap.solution.entity.enums.ETipoCurso;

public class CursoDAO extends GenericDAO<Curso, Integer> {

	public CursoDAO(EntityManager em) {
		super(em);
	}

	public List<Curso> buscarPorNomeTipoEModalidade(String nome, ETipoCurso tipo, EModalidade modalidade) {
		
		String consultaSql = "select c from Curso c "
				+"where c.nome = :nome and "
				+"c.tipoCurso = :tipoCurso and "
				+"c.modalidade = :modalidade";
		
		return this.em.createQuery(consultaSql, Curso.class)
				.setParameter("nome", nome)
				.setParameter("tipoCurso", tipo)
				.setParameter("modalidade", modalidade)
				.setMaxResults(1).getResultList();
		
	}

}
