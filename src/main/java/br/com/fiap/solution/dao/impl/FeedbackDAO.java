package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Feedback;
import br.com.fiap.solution.entity.Projeto;

public class FeedbackDAO extends GenericDAO<Feedback, Integer> {

	public FeedbackDAO(EntityManager em) {
		super(em);
	}

	public List<Feedback> buscarPorProjeto(Projeto projeto) {
		String consultaSql = "from Feedback f "
				+"where f.projeto = :projeto";
		
		return this.em.createQuery(consultaSql, Feedback.class)
				.setParameter("projeto", projeto)
				.getResultList();
	}
	
	public List<Object> agruparNotaPorProjeto(Projeto projeto) {
		String consultaSql = "select count(f) from Feedback f "
				+"where f.projeto = :projeto " +
				"group by f.nota";
		
		return this.em.createQuery(consultaSql, Object.class)
				.setParameter("projeto", projeto)
				.getResultList();
	}
	

}
