package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Grupo;
import br.com.fiap.solution.entity.Projeto;

public class GrupoDAO extends GenericDAO<Grupo, Integer> {

	public GrupoDAO(EntityManager em) {
		super(em);
	}

	public List<Grupo> buscarPorProjeto(Projeto projeto) {
		String consultaSql = "from Grupo g "
				+"where g.projeto = :projeto";
		
		return this.em.createQuery(consultaSql, Grupo.class)
				.setParameter("projeto", projeto)
				.setMaxResults(1).getResultList();
	}
	
}
