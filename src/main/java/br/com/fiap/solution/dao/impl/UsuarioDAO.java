package br.com.fiap.solution.dao.impl;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Usuario;

public class UsuarioDAO extends GenericDAO<Usuario, Integer> {

	public UsuarioDAO(EntityManager em) {
		super(em);
	}

}
