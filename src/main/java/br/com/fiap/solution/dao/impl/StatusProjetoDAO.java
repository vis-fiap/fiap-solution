package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.StatusProjeto;

public class StatusProjetoDAO extends GenericDAO<StatusProjeto, Integer> {

	public StatusProjetoDAO(EntityManager em) {
		super(em);
	}

	public List<StatusProjeto> buscarPorDescricao(String descricao) {
		String consultaSql = "from StatusProjeto s where s.descricao = :descricao";
		
		return this.em.createQuery(consultaSql, StatusProjeto.class)
				.setParameter("descricao", descricao)
				.setMaxResults(1).getResultList();
		
	}

}
