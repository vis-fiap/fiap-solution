package br.com.fiap.solution.dao.impl;

import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Professor;

public class ProfessorDAO extends GenericDAO<Professor, Integer>{

	public ProfessorDAO(EntityManager em) {
		super(em);
	}
	
	@Override
	public List<Professor> listar(){
		return em.createQuery("from Professor p "
				+ "where p.dtDesligamento < :data or p.dtDesligamento IS NULL", Professor.class)
				.setParameter("data", new GregorianCalendar()).getResultList();
	}
	
}
