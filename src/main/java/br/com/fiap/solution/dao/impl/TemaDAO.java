package br.com.fiap.solution.dao.impl;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Tema;

public class TemaDAO extends GenericDAO<Tema, Integer> {

	public TemaDAO(EntityManager em) {
		super(em);
	}

}
