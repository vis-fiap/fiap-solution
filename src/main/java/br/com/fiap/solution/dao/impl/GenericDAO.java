package br.com.fiap.solution.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;

import br.com.fiap.solution.exception.DBCommitException;
import br.com.fiap.solution.exception.IdNotFoundException;

public abstract class GenericDAO<T, C> {

protected Class<T> classeEntidade;
	
	protected EntityManager em;
	
	@SuppressWarnings("unchecked")
	public GenericDAO(EntityManager em) {
        this.em = em;
        this.classeEntidade = (Class<T>) ((ParameterizedType) this.getClass()
        		.getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public void cadastrar(T entidade) throws DBCommitException {
		try {
			em.getTransaction().begin();
			em.persist(entidade);
			em.getTransaction().commit();
		} catch (Exception e) {
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			e.printStackTrace();
			throw new DBCommitException("Erro ao persistir");
		}
	}
	
	public T buscarPorChave(C chave) {
		return this.em.find(classeEntidade, chave);
		
	}
	
	public List<T> listar() {
		CriteriaQuery<T> query = this.em.getCriteriaBuilder().createQuery(this.classeEntidade);
		CriteriaQuery<T> select = query.select(query.from(this.classeEntidade));
		return this.em.createQuery(select).getResultList();
	}

	public T buscarUltimoRegistro() {
		List<T> lista = listar();
		return lista.get(lista.size() - 1);
	}
	
	public void excluir(C chave) throws IdNotFoundException, DBCommitException {
		
		T entidadeExcluir = this.buscarPorChave(chave);
		if (entidadeExcluir == null) {
			String mensagemErro = "Nenhum registro de %s encontrado com a chave %s";
			throw new IdNotFoundException(
					String.format(mensagemErro, this.classeEntidade.getSimpleName(), chave));
		}
		
		try{
			em.getTransaction().begin();
			em.remove(entidadeExcluir);
			em.getTransaction().commit();
		}catch(Exception e){
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new DBCommitException("Erro ao Remover");
		}
		
	}
	
	public void atualizar(T entity) throws Exception {
		try{
			em.getTransaction().begin();
			em.merge(entity);
			em.getTransaction().commit();
		}catch(Exception e){
			if (em.getTransaction().isActive())
				em.getTransaction().rollback();
			throw new DBCommitException("Erro ao atualizar");
		}
	}

}
