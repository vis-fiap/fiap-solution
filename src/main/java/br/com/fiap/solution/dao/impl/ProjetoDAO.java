package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Projeto;

public class ProjetoDAO extends GenericDAO<Projeto, Integer> {

	public ProjetoDAO(EntityManager em) {
		super(em);
	}

	public List<Projeto> buscarPorTitulo(String titulo) {
		String consultaSql = "from Projeto p "
				+"where p.titulo = :titulo";
		
		return this.em.createQuery(consultaSql, Projeto.class)
				.setParameter("titulo", titulo)
				.setMaxResults(1).getResultList();
		
	}

}
