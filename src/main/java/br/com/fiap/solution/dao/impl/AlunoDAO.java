package br.com.fiap.solution.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.fiap.solution.entity.Aluno;
import br.com.fiap.solution.entity.Turma;

public class AlunoDAO extends GenericDAO<Aluno, Integer> {

	public AlunoDAO(EntityManager em) {
		super(em);
	}

	public List<Aluno> consultarPorRM(Integer rm) {
		return em.createQuery("from Aluno a where a.rm=:rm", Aluno.class).setParameter("rm", rm).setMaxResults(1).getResultList();
	}
	
	public List<Aluno> listarAlunosPorTurma(Turma turma){
		return em.createQuery("from Aluno a where a.turma = :turma", Aluno.class).setParameter("turma", turma).getResultList();
	}

}
