package br.com.fiap.solution.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.fiap.solution.util.DadosTeste;

@Entity
@Table(name = "tbfs_feedback")
public class Feedback {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_feedback")
	private Integer id;
	
	@Column(name = "nr_nota", nullable = false)
	private Integer nota;

	@Column(name = "ds_comentario")
	private String comentario;
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dt_cadastro")
	private Calendar dtCadastro;

	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name="id_projeto")
	private Projeto projeto;
	
	public Feedback() {
		super();
	}

	public Feedback(Integer id, String comentario, Integer nota, Calendar dtCadastro, Usuario usuario,
			Projeto projeto) {
		super();
		this.id = id;
		this.comentario = comentario;
		this.nota = nota;
		this.dtCadastro = dtCadastro;
		this.usuario = usuario;
		this.projeto = projeto;
	}

	public Feedback(Integer nota, String comentario, Usuario usuario, Projeto projeto) {
		super();
		this.nota = nota;
		this.comentario = comentario;
		this.usuario = usuario;
		this.projeto = projeto;
	}
	
	public Feedback( Integer nota, Usuario usuario, Projeto projeto) {
		super();
		this.nota = nota;
		this.usuario = usuario;
		this.projeto = projeto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public Calendar getDtCadastro() {
		return dtCadastro;
	}
	
	public String getDtCadastroFormatado() {
		return DadosTeste.formatarData(dtCadastro);
	}

	public void setDtCadastro(Calendar dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}

	@Override
	public String toString() {
		return "Feedback [id=" + id + ", comentario=" + comentario + ", nota=" + nota + ", dtCadastro=" + getDtCadastroFormatado()
				+ ", usuario=" + usuario.getApelido().toString() + ", projeto=" + projeto.getTitulo().toString() + "]";
	}

	

}
