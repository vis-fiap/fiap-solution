package br.com.fiap.solution.entity.enums;

public enum ETipoCurso {

	GRADUCAO("GRADUAÇÃO"),
	MBA("MBA");
	
	private String descricao;
	
	ETipoCurso(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
