package br.com.fiap.solution.entity.enums;

public enum EGenero {
	FEMININO("FEMININO"),
	MASCULINO("MASCULINO"),
	INDEFINIDO("INDEFINIDO");
	
	private String descricao;
	
	EGenero(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
}
