package br.com.fiap.solution.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.fiap.solution.util.DadosTeste;

@Entity
@Table(name = "tbfs_grupo")
public class Grupo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_grupo")
	private Integer id;
	
	@Column(name = "nm_grupo", nullable = false, length = 40)
	private String nome;
	
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "dt_criacao")
	private Calendar dtCriacao;

	@OneToMany(mappedBy = "grupo")
	private List<Aluno> lstAluno;

	@OneToOne(mappedBy = "grupo")
	private Projeto projeto;
	
	public Grupo() {
		super();
	}

	public Grupo(Integer id, String nome, Calendar dtCriacao, List<Aluno> lstAluno, Projeto projeto) {
		super();
		this.id = id;
		this.nome = nome;
		this.dtCriacao = dtCriacao;
		this.lstAluno = lstAluno;
		this.projeto = projeto;
	}

	public Grupo(Integer id, String nome, Calendar dtCriacao, List<Aluno> lstAluno) {
		super();
		this.id = id;
		this.nome = nome;
		this.dtCriacao = dtCriacao;
		this.lstAluno = lstAluno;
	}
	
	public Grupo(String nome, List<Aluno> lstAluno) {
		super();
		this.nome = nome;
		this.lstAluno = lstAluno;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDtCriacao() {
		return dtCriacao;
	}
	
	public String getDtCriacaoFormatada() {
		return DadosTeste.formatarData(dtCriacao);
	}

	public void setDtCriacao(Calendar dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public List<Aluno> getLstAluno() {
		return lstAluno;
	}

	public void setLstAluno(List<Aluno> lstAluno) {
		this.lstAluno = lstAluno;
	}
	
	public Projeto getProjeto() {
		return projeto;
	}

	public void setProjeto(Projeto projeto) {
		this.projeto = projeto;
	}
	
	@Override
	public String toString() {
		return "Grupo [id=" + id + ", nome=" + nome + ", dtCriacao=" + getDtCriacaoFormatada() + ", lstAluno=" + lstAluno +  ", Projeto =" + projeto + "]";
	}
	
}
