package br.com.fiap.solution.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbfs_tema")
public class Tema {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tema")
	private Integer id;

	@Column(name = "nm_tema", nullable = false, length = 60)
	private String nome;

	public Tema() {
		super();
	}

	public Tema(Integer id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}
	
	public Tema(String nome) {
		super();
		this.nome = nome;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Override
	public String toString() {
		return "TemaProjeto [id=" + id + ", descricao=" + nome + "]";
	}

}
