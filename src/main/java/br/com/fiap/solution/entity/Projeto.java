package br.com.fiap.solution.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.fiap.solution.util.DadosTeste;

@Entity
@Table(name = "tbfs_projeto")
public class Projeto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_projeto")
	private Integer id;
	
	@Column(name = "ds_titulo", nullable = false, length=60)
	private String titulo;
	
	@Column(name = "ds_projeto", nullable = false)
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "id_tema")
	private Tema tema;
	
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name="dt_criacao")
	private Calendar dtCriacao;
	
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name="dt_atualizacao")
	private Calendar dtAtualizacao;
	
	@Column(name = "op_proj_publico", nullable = false)
	private boolean projetoPublico;
	
	@Column(name = "ds_url_repositorio")
	private String urlRepositorio;
	
	@Column(name = "ds_url_pitch")
	private String urlPitch;
	
	@ManyToOne
	@JoinColumn(name="id_status_proj")
	private StatusProjeto status;
	
	@OneToMany(mappedBy="projeto")
	private List<Feedback> lstFeedback;
	
	@OneToOne
	@JoinColumn(name="id_grupo")
	private Grupo grupo;

	public Projeto() {
		super();
	}

	public Projeto(Integer id, String titulo, String descricao, Tema tema, Calendar dtCriacao, Calendar dtAtualizacao,
			boolean projetoPublico, String urlRepositorio, String urlPitch, StatusProjeto status,
			List<Feedback> lstFeedback, Grupo grupo) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.descricao = descricao;
		this.tema = tema;
		this.dtCriacao = dtCriacao;
		this.dtAtualizacao = dtAtualizacao;
		this.projetoPublico = projetoPublico;
		this.urlRepositorio = urlRepositorio;
		this.urlPitch = urlPitch;
		this.status = status;
		this.lstFeedback = lstFeedback;
		this.grupo = grupo;
	}
	
	public Projeto(String titulo, String descricao, Tema tema, boolean projetoPublico, String urlRepositorio,
			String urlPitch, StatusProjeto status, Grupo grupo) {
		super();
		this.titulo = titulo;
		this.descricao = descricao;
		this.tema = tema;
		this.projetoPublico = projetoPublico;
		this.urlRepositorio = urlRepositorio;
		this.urlPitch = urlPitch;
		this.status = status;
		this.grupo = grupo;
	}
	
	public Projeto(String titulo, String descricao, Tema tema, boolean projetoPublico, String urlPitch,
			StatusProjeto status, Grupo grupo) {
		super();
		this.titulo = titulo;
		this.descricao = descricao;
		this.tema = tema;
		this.projetoPublico = projetoPublico;
		this.urlPitch = urlPitch;
		this.status = status;
		this.grupo = grupo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Tema getTema() {
		return tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	public Calendar getDtCriacao() {
		return dtCriacao;
	}
	
	public String getDtCriacaoFormatado() {
		return DadosTeste.formatarData(dtCriacao);
	}

	public void setDtCriacao(Calendar dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	public Calendar getDtAtualizacao() {
		return dtAtualizacao;
	}

	public String getDtAtualizacaoFormatado() {
		return DadosTeste.formatarData(dtAtualizacao);
	}
	
	public void setDtAtualizacao(Calendar dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public boolean isProjetoPublico() {
		return projetoPublico;
	}

	public void setProjetoPublico(boolean projetoPublico) {
		this.projetoPublico = projetoPublico;
	}

	public String getUrlRepositorio() {
		return urlRepositorio;
	}

	public void setUrlRepositorio(String urlRepositorio) {
		this.urlRepositorio = urlRepositorio;
	}

	public String getUrlPitch() {
		return urlPitch;
	}

	public void setUrlPitch(String urlPitch) {
		this.urlPitch = urlPitch;
	}

	public StatusProjeto getStatus() {
		return status;
	}

	public void setStatus(StatusProjeto status) {
		this.status = status;
	}

	public List<Feedback> getLstFeedback() {
		return lstFeedback;
	}

	public void setLstFeedback(List<Feedback> lstFeedback) {
		this.lstFeedback = lstFeedback;
	}

	public Grupo getGrupo() {
		return grupo;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	@Override
	public String toString() {
		return "Projeto [id=" + id + ", titulo=" + titulo + ", descricao=" + descricao + ", tema=" + tema
				+ ", dtCriacao=" + getDtCriacaoFormatado() + ", dtAtualizacao=" + getDtAtualizacaoFormatado() + ", projetoPublico=" + projetoPublico
				+ ", urlRepositorio=" + urlRepositorio + ", urlPitch=" + urlPitch + ", status=" + status
				+ ", lstFeedback=" + lstFeedback + ", grupo=" + grupo + "]";
	}
	
}
