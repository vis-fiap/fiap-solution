package br.com.fiap.solution.entity;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.fiap.solution.entity.enums.EGenero;
import br.com.fiap.solution.util.DadosTeste;

@Entity
@Table(name = "tbfs_professor")
public class Professor extends Pessoa {

	@Temporal(TemporalType.DATE)
	@Column(name = "dt_contratacao")
	private Calendar dtContratacao;

	@Temporal(TemporalType.DATE)
	@Column(name = "dt_desligamento")
	private Calendar dtDesligamento;
	
	@ManyToMany(mappedBy="lstProfessor")
	private List<Turma> lstTurma;
	
	public Professor() {
		super();
	}

	public Professor(Calendar dtContratacao, Calendar dtDesligamento) {
		super();
		this.dtContratacao = dtContratacao;
		this.dtDesligamento = dtDesligamento;
	}

	public Professor(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero,
			Byte[] foto, Calendar dtContratacao, Calendar dtDesligamento) {
		super(nome, dtNascimento, email, linkedin, genero, foto);
		this.dtContratacao = dtContratacao;
		this.dtDesligamento = dtDesligamento;
	}
	
	public Professor(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero,
			Byte[] foto, Calendar dtContratacao, Usuario usuario) {
		super(nome, dtNascimento, email, linkedin, genero, foto, usuario);
		this.dtContratacao = dtContratacao;
	}

	public String getDtContratacao() {
		return DadosTeste.formatarData(dtContratacao);
	}

	public void setDtContratacao(Calendar dtContratacao) {
		this.dtContratacao = dtContratacao;
	}

	public String getDtDesligamento() {
		return DadosTeste.formatarData(dtDesligamento);
	}

	public void setDtDesligamento(Calendar dtDesligamento) {
		this.dtDesligamento = dtDesligamento;
	}

	@Override
	public String toString() {
		return "Professor [id=" + id + ", nome=" + nome + ", dtContratacao=" + getDtContratacao() + ", dtDesligamento=" + getDtDesligamento() 
				+  ", dtNascimento=" + getDtNascimento() + ", email=" + email + ", linkedin=" + linkedin
				+ ", genero=" + genero + ", foto=" + Arrays.toString(foto) + ", usuario=" + usuario + "]";
	}

}
