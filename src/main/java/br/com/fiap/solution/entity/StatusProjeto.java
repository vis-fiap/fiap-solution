package br.com.fiap.solution.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbfs_status_projeto")
public class StatusProjeto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_status_proj")
	private Integer id;
	
	@Column(name = "ds_status_proj", nullable = false, length = 40)
	private String descricao;
	
	@OneToMany(mappedBy="status")
	private List<Projeto> lstProjeto;

	public StatusProjeto() {
		super();
	}

	public StatusProjeto(String descricao) {
		super();
		this.descricao = descricao;
	}
	
	public StatusProjeto(Integer id, String descricao, List<Projeto> lstProjeto) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.lstProjeto = lstProjeto;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public List<Projeto> getLstProjeto() {
		return lstProjeto;
	}

	public void setLstProjeto(List<Projeto> lstProjeto) {
		this.lstProjeto = lstProjeto;
	}
	
	@Override
	public String toString() {
		return "StatusProjeto [id=" + id + ", descricao=" + descricao + "]";
	}
	
}
