package br.com.fiap.solution.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tbfs_usuario")
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private Integer idUsuario;

	@Column(name = "ds_apelido", length = 30, nullable = false)
	private String apelido;

	@Column(name = "ds_senha", length = 20, nullable = false)
	private String senha;

	@OneToMany(mappedBy = "usuario")
	private List<Feedback> lstFeedback;

	@OneToOne(mappedBy = "usuario")
	private Pessoa pessoa;

	public Usuario() {
		super();
	}

	public Usuario(Integer idUsuario, String apelido, String senha, List<Feedback> lstFeedback, Pessoa pessoa) {
		super();
		this.idUsuario = idUsuario;
		this.apelido = apelido;
		this.senha = senha;
		this.lstFeedback = lstFeedback;
		this.pessoa = pessoa;
	}

	public Usuario(String apelido, String senha, Pessoa pessoa) {
		super();
		this.apelido = apelido;
		this.senha = senha;
		this.pessoa = pessoa;
	}

	public Usuario(String apelido, String senha) {
		super();
		this.apelido = apelido;
		this.senha = senha;
	}
	
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Feedback> getLstFeedback() {
		return lstFeedback;
	}

	public void setLstFeedback(List<Feedback> lstFeedback) {
		this.lstFeedback = lstFeedback;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

}