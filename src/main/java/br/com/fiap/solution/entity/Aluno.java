package br.com.fiap.solution.entity;

import java.util.Arrays;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.fiap.solution.entity.enums.EGenero;
import br.com.fiap.solution.util.DadosTeste;

@Entity
@Table(name = "tbfs_aluno")
public class Aluno extends Pessoa {

	@Column(name = "nr_rm", unique=true)
	private Integer rm;
	
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "dt_cadastro")
	private Calendar dtCadastro;

	@ManyToOne
	@JoinColumn(name="id_turma")
	private Turma turma;
	
	@ManyToOne
	@JoinColumn(name="id_grupo")
	private Grupo grupo;
	
	public Aluno() {
		super();
	}

	public Aluno(Integer rm, Calendar dtCadastro) {
		super();
		this.rm = rm;
		this.dtCadastro = dtCadastro;
	}
	
	public Aluno(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero, Byte[] foto, Integer rm, Turma turma) {
		super(nome, dtNascimento, email, linkedin, genero, foto);
		this.rm = rm;
		this.turma = turma;
	}
	
	public Aluno(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero, Integer rm, Turma turma, Usuario usuario) {
		super(nome, dtNascimento, email, linkedin, genero, usuario);
		this.rm = rm;
		this.turma = turma;
	}

	public Integer getRm() {
		return rm;
	}

	public void setRm(Integer rm) {
		this.rm = rm;
	}

	public String getDtCadastro() {
		return DadosTeste.formatarData(dtCadastro);
	}

	@Override
	public String toString() {
		return "Aluno [rm=" + rm + ", dtCadastro=" + getDtCadastro() + ", turma=" + turma + ", grupo=" + grupo + ", id=" + id
				+ ", nome=" + nome + ", dtNascimento=" + getDtNascimento() + ", email=" + email + ", linkedin=" + linkedin
				+ ", genero=" + genero + ", foto=" + Arrays.toString(foto) + ", usuario=" + usuario + "]";
	}

	public void setDtCadastro(Calendar dtCadastro) {
		this.dtCadastro = dtCadastro;
	}
	
	

}
