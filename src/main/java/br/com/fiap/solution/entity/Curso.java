package br.com.fiap.solution.entity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.fiap.solution.entity.enums.EModalidade;
import br.com.fiap.solution.entity.enums.ETipoCurso;

@Entity
@Table(name = "tbfs_curso")
public class Curso {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_curso")
	private Integer id;
	
	@Column(name = "nm_curso", nullable = false, length = 60)
	private String nome;
	
	@Column(name = "ds_curso", nullable = false)
	private String descricao;
	
	@Column(name = "nr_meses", nullable = false)
	private Integer duracaoMeses;
	
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "dt_cadastro_curso")
	private Calendar dtCadastro;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tp_curso", length = 20, nullable=false)
	private ETipoCurso tipoCurso;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "nm_modalidade", length = 20, nullable=false)
	private EModalidade modalidade;

	@OneToMany(mappedBy="curso")
	private List<Turma> turma;
	
	public Curso() {
		super();
	}

	public Curso(Integer id, String nome, String descricao, Integer duracaoMeses, Calendar dtCadastro, EModalidade modalidade,
			ETipoCurso tipoCurso, List<Turma> turma) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.dtCadastro = dtCadastro;
		this.modalidade = modalidade;
		this.duracaoMeses = duracaoMeses;
		this.tipoCurso = tipoCurso;
		this.turma = turma;
	}

	public Curso(String nome, String descricao, ETipoCurso tipoCurso, EModalidade modalidade, Integer duracaoMeses) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.tipoCurso = tipoCurso;
		this.modalidade = modalidade;
		this.duracaoMeses = duracaoMeses;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Calendar getDtCadastro() {
		return dtCadastro;
	}
	
	public String getDtCadastroFormatado() {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.format(dtCadastro.getTime());
	}

	public void setDtCadastro(Calendar dtCadastro) {
		this.dtCadastro = dtCadastro;
	}

	public EModalidade getModalidade() {
		return modalidade;
	}

	public void setModalidade(EModalidade modalidade) {
		this.modalidade = modalidade;
	}

	public Integer getDuracaoMeses() {
		return duracaoMeses;
	}

	public void setDuracaoMeses(Integer duracaoMeses) {
		this.duracaoMeses = duracaoMeses;
	}

	public ETipoCurso getTipoCurso() {
		return tipoCurso;
	}

	public void setTipoCurso(ETipoCurso tipoCurso) {
		this.tipoCurso = tipoCurso;
	}

	public List<Turma> getTurma() {
		return turma;
	}

	public void setTurma(List<Turma> turma) {
		this.turma = turma;
	}
	
	
	
	@Override
	public String toString() {
		return "Curso [id=" + id + ", nome=" + nome + ", descricao=" + descricao + ", duracaoMeses=" + duracaoMeses
				+ ", dtCadastro=" + getDtCadastroFormatado() + ", tipoCurso=" + tipoCurso + ", modalidade=" + modalidade + "]";
	}
	
}
