package br.com.fiap.solution.entity;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

import br.com.fiap.solution.util.DadosTeste;

@Entity
@Table(name = "tbfs_turma")
public class Turma {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_turma")
	private Integer id;

	@Column(name = "nm_turma", length = 30, nullable = false, unique = true)
	private String nomeTurma;

	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	@Column(name = "dt_inicio")
	private Calendar dtInicio;

	@Temporal(TemporalType.DATE)
	@Column(name = "dt_fim")
	private Calendar dtFim;

	@ManyToOne
	@JoinColumn(name = "id_curso")
	private Curso curso;

	@OneToMany(mappedBy = "turma")
	private List<Aluno> lstAluno;

	@ManyToMany
	@JoinTable(joinColumns = @JoinColumn(name = "id_turma"), 
	inverseJoinColumns = @JoinColumn(name = "id_pessoa"), 
	name = "tbfs_avaliador")
	private List<Professor> lstProfessor;

	public Turma() {
		super();
	}
	
	public Turma(Integer id, String nomeTurma, Calendar dtInicio, Calendar dtFim, Curso curso, List<Aluno> lstAluno,
			List<Professor> lstProfessor) {
		super();
		this.id = id;
		this.nomeTurma = nomeTurma;
		this.dtInicio = dtInicio;
		this.dtFim = dtFim;
		this.curso = curso;
		this.lstAluno = lstAluno;
		this.lstProfessor = lstProfessor;
	}
	
	public Turma(String nomeTurma, Calendar dtInicio, int duracaoMeses, Curso curso, List<Professor> lstProfessor) {
		super();
		this.nomeTurma = nomeTurma;
		this.dtInicio = dtInicio;
		setDtFim(duracaoMeses);
		this.curso = curso;
		this.lstProfessor = lstProfessor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeTurma() {
		return nomeTurma;
	}

	public void setNomeTurma(String nomeTurma) {
		this.nomeTurma = nomeTurma;
	}

	public Calendar getDtInicio() {
		return dtInicio;
	}
	
	public String getDtInicioFormatado() {
		return DadosTeste.formatarData(dtInicio);
	}

	public void setDtInicio(Calendar dtInicio) {
		this.dtInicio = dtInicio;
	}

	public Calendar getDtFim() {
		return dtFim;
	}
	
	public String getDtFimFormatado() {
		return DadosTeste.formatarData(dtFim);
	}

	public void setDtFim(Calendar dtFim) {
		this.dtFim = dtFim;
	}
	
	public void setDtFim(int duracaoMeses) {
		this.dtFim = DadosTeste.adicionarMesesData(dtInicio, duracaoMeses);
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Aluno> getLstAluno() {
		return lstAluno;
	}

	public void setLstAluno(List<Aluno> lstAluno) {
		this.lstAluno = lstAluno;
	}

	public List<Professor> getLstProfessor() {
		return lstProfessor;
	}

	public void setLstProfessor(List<Professor> lstProfessor) {
		this.lstProfessor = lstProfessor;
	}

	@Override
	public String toString() {
		return "Turma [id=" + id + ", nomeTurma=" + nomeTurma + ", dtInicio=" + getDtInicioFormatado() + ", dtFim=" + getDtFimFormatado()
				+ ", curso=" + curso.toString() + ", lstProfessor=" + lstProfessor + "]";
	}

}
