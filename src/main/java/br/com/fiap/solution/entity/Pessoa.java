package br.com.fiap.solution.entity;

import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.fiap.solution.entity.enums.EGenero;
import br.com.fiap.solution.util.DadosTeste;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@Table(name = "tbfs_pessoa")
public class Pessoa {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_pessoa")
	protected Integer id;
	
	@Column(name = "nm_pessoa", length = 60, nullable=false)
	protected String nome;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dt_nascimento")
	protected Calendar dtNascimento;
	
	@Column(name = "ds_email_pessoa", length = 100, unique=true)
	protected String email;
	
	@Column(name = "ds_linkedin", length = 120)
	protected String linkedin;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "ds_genero", nullable=false)
	protected EGenero genero;
	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name = "ds_foto")
	protected Byte[] foto;

	@OneToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="id_usuario")
	protected Usuario usuario;
	
	public Pessoa() {
		super();
	}

	public Pessoa(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero,
			Byte[] foto) {
		super();
		this.nome = nome;
		this.dtNascimento = dtNascimento;
		this.email = email;
		this.linkedin = linkedin;
		this.genero = genero;
		this.foto = foto;
	}
	
	public Pessoa(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero,
			Byte[] foto, Usuario usuario) {
		super();
		this.nome = nome;
		this.dtNascimento = dtNascimento;
		this.email = email;
		this.linkedin = linkedin;
		this.genero = genero;
		this.foto = foto;
		this.usuario = usuario;
	}

	public Pessoa(String nome, Calendar dtNascimento, String email, String linkedin, EGenero genero,
			 Usuario usuario) {
		super();
		this.nome = nome;
		this.dtNascimento = dtNascimento;
		this.email = email;
		this.linkedin = linkedin;
		this.genero = genero;
		this.usuario = usuario;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDtNascimento() {
		return DadosTeste.formatarData(dtNascimento);
	}

	public void setDtNascimento(Calendar dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		this.linkedin = linkedin;
	}

	public EGenero getGenero() {
		return genero;
	}

	public void setGenero(EGenero genero) {
		this.genero = genero;
	}

	public Byte[] getFoto() {
		return foto;
	}

	public void setFoto(Byte[] foto) {
		this.foto = foto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
