package br.com.fiap.solution.entity.enums;

public enum EStatus {

	NOVO("NOVO"),
	EM_ANALISE("EM ANÁLISE"),
	PUBLICADO("PUBLICADO"),
	PENDENTE_CORRECAO("PENDENTE CORREÇÃO"),
	REVISADO("REVISADO");
	
	private String descricao;
	
	EStatus(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
