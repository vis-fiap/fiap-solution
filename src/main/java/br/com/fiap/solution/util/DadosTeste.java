package br.com.fiap.solution.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DadosTeste {
	
	private static final String LINHA = "============================================================";
	private static final String TITULO = "============ %s ============";
	
	public static void logarLinha() {
		System.out.println(LINHA);
	}

	public static void logarTitulo(String mensagem) {
		System.out.println(String.format(TITULO, mensagem));
	}
	
	public static String formatarData(Calendar data) {
		DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return (String) ((data!=null)?df.format(data.getTime()):data);
	}
	
	public static Calendar adicionarMesesData(Calendar data, int qtdMeses) {
		Calendar novaData = data;
		novaData.add(Calendar.MONTH, qtdMeses);
		return novaData;
	}

}
