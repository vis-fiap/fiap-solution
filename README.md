# fiap-solution
[![Generic badge](https://img.shields.io/badge/Language-JAVA-orange.svg)](https://gitlab.com/vis-fiap/fiap-solution)
[![Generic badge](https://img.shields.io/badge/Use-JPA-blue.svg)](https://gitlab.com/vis-fiap/fiap-solution)
[![Generic badge](https://img.shields.io/badge/Use-JPQL-green.svg)](https://gitlab.com/vis-fiap/fiap-solution)

### Fase 1 - Atividade sobre Java Foundations & UX

O projeto apresenta as funções do portal de soluções para alunos submeterem os projetos/soluções para avaliação. A classe principal realiza um CRUD (criar, ler, atualizar e deletar) em cada entidade do sistema para validação das funcionalidades.
 * **Classe principal:** [FiapSolutionTeste.java](src/main/java/br/com/fiap/solution/teste/FiapSolutionTeste.java)

#### Protótipo do Portal:
Figma com página web e mobile do [portal de soluções](https://www.figma.com/proto/ZvrvEitpZNvDTrO0nfbRDl/Fiap-Solutions?node-id=1%3A3&scaling=min-zoom&page-id=0%3A1).

----

### :pushpin: Comandos para iniciar o banco de dados `MySql` da aplicação
Além de provisionar o ambiente do banco de dados `MYSQL`, cria o database `fiap_solution` que é utilizado pela aplicação.

```bash
$ docker run --name mysql-teste -p 3306:3306 -p 33060:33060 -e MYSQL_ROOT_PASSWORD=admin \
-e MYSQL_DATABASE=fiap_solution -d mysql

```
